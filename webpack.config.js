
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './client/index.html',
  filename: 'index.html',
  inject: 'body'
})

let path = require("path");

module.exports = env=> {

  let PRODUCTION = false
  
    // env is passed via ``` webpack --env.production ```
    if (env && env.production) {
      PRODUCTION = true
    }
  
    let publicPath = "/"
  
    // Make the root be relative in production
    if (PRODUCTION) {
      publicPath = "./"
    } 
  return {
    entry: './client/index.js',
    output: {
      path: path.resolve(__dirname, "dist"),
      publicPath: publicPath,
      filename: 'index_bundle.js'
    },
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query: {
            presets: ['react', 'env']
          }
        }

      ]
    },
    plugins: [HtmlWebpackPluginConfig]
  }
}