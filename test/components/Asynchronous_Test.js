
import React from 'react'
import {expect} from 'chai'
import {render} from 'enzyme'

import Asynchronous from '../../client/components/Asynchronous.jsx'
import {
    ASYNCHRONOUS_LOADING,
    ASYNCHRONOUS_ERROR ,
    ASYNCHRONOUS_SUCCESS,
  } from '../../client/ducks/SynchronousDuck'

describe('<ASynchronous />', ()=>{

    let component = undefined
    let node = undefined

    context("loading", ()=>{
        before( ()=>{
            component = render(
                <Asynchronous
                    status="Loading"
                    items={ {key: 'value'} }
                    />
            )
            node = component.find( 'li' )
        })

        it('has status of loading', ()=>{
            expect( component.find('h2').text() ).to.equal('Status: Loading')
        })

        it('has li node', ()=>{
            expect( node.length ).to.equal(1)
        })

        it('has an li with a=1', ()=>{
            expect( node.html() ).to.equal('key = value')
        })
    })

})