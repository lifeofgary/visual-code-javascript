
import React from 'react'
import {expect} from 'chai'
import sinon from 'sinon'
import {shallow} from 'enzyme'

import Synchronous from '../../client/components/Synchronous.jsx'
import {
    SYNCHRONOUS_LOADING,
    SYNCHRONOUS_ERROR ,
    SYNCHRONOUS_SUCCESS,
  } from '../../client/ducks/SynchronousDuck'

describe('<Synchronous />', ()=>{

    let component = undefined
    let node = undefined

    context("loading", ()=>{
        let spy = sinon.spy()

        before( ()=>{
            component = shallow(
                <Synchronous
                    step={SYNCHRONOUS_LOADING}
                    onLoading={spy}
                    />
            )
            node = component.find('.loading') 
        })

        it('has status of loading', ()=>{
            expect( component.find('h2').text() ).to.equal('Status: Loading')
        })

        it('has node', ()=>{ 
            expect( node.length ).to.equal(1)
        })

        it('responds to onLoading', ()=>{
            node.simulate('click');
            expect( spy.called ).to.be.true    
        })
    })


    context("error", ()=>{
        let spy = sinon.spy()

        before( ()=>{
            component = shallow(
                <Synchronous
                    step={SYNCHRONOUS_ERROR}
                    onError={spy}
                    />
            )
            node = component.find('.error') 
        })

        it('has status of Error', ()=>{
            expect( component.find('h2').text() ).to.equal('Status: Error')
        })

        it('has node', ()=>{ 
            expect( node.length ).to.equal(1)
        })

        it('responds to onError', ()=>{
            node.simulate('click');
            expect( spy.called ).to.be.true    
        })
    })
    
    context("success", ()=>{
        let spy = sinon.spy()

        before( ()=>{
            component = shallow(
                <Synchronous
                    step={SYNCHRONOUS_SUCCESS}
                    onSuccess={spy}
                    />
            )
            node = component.find('.success') 
        })

        it('has status of Success', ()=>{
            expect( component.find('h2').text() ).to.equal('Status: Success')
        })

        it('has node', ()=>{ 
            expect( node.length ).to.equal(1)
        })

        it('responds to onSuccess', ()=>{
            node.simulate('click');
            expect( spy.called ).to.be.true    
        })
    })    
        
})