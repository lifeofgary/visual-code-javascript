

// configure enzyme
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

// configure sinon promises
import sinon from 'sinon'
import  sinonStubPromise from 'sinon-stub-promise'
sinonStubPromise(sinon)

