import {expect} from 'chai'
import sinon from 'sinon'

import {
  asynchronousLoading,
  asynchronousError,
  asynchronousSuccess,
  ASYNCHRONOUS_ERROR,
  ASYNCHRONOUS_LOADING,
  ASYNCHRONOUS_SUCCESS,

  asynchronousFetchLogic,
} from '../../client/ducks/AsynchronousDuck'

import reducer from '../../client/ducks/AsynchronousDuck' 

describe('SynchronousDuck', ()=>{
  
  describe('actions', ()=>{
      
    it('asynchronousLoading', ()=>{
      expect( asynchronousLoading().type).to.equal( ASYNCHRONOUS_LOADING )
    })
  
    it('asynchronousError', ()=>{
      expect( asynchronousError().type).to.equal( ASYNCHRONOUS_ERROR )
    })
  
    it('asynchronousSuccess', ()=>{
      expect( asynchronousSuccess().type).to.equal( ASYNCHRONOUS_SUCCESS )
    })

    describe('asynchronousFetchLogic', ()=>{
      let promise
      let dispatch

      beforeEach( ()=>{
        promise = sinon.stub().returnsPromise();
        dispatch=sinon.spy()
      })

      it('dispatchs a error', ()=>{
        promise.resolves({ ok: false})
        asynchronousFetchLogic(promise(), dispatch)

        expect( dispatch.called ).to.be.true
        // args[0][0] is the first parameter
        expect( dispatch.args[0][0].type ).to.equal( asynchronousError().type )
      })

      it('dispatchs successfully', ()=>{
        promise.resolves({
          ok: true,
          json: ()=>[1,2,3]
        })
        asynchronousFetchLogic(promise(), dispatch)

        expect( dispatch.called ).to.be.true
        // args[0][0] is the first call, first parameter
        expect( dispatch.args[0][0].type ).to.equal( ASYNCHRONOUS_LOADING )
        // args[1][1] is the second call, second parameter
        expect( dispatch.args[1][0] ).to.deep.equal({
          type: ASYNCHRONOUS_SUCCESS,
          payload: [1,2,3]
        })
      })
    })
  })

  describe('reducer', ()=>{
    it('initializes', ()=>{
      let result = reducer( undefined ,{type: 'unknown event' })
      expect( result.status ).to.equal( 'Loading' )
        expect( result.items.length ).to.equal( 0 )
      })

    it('type ASYNCHRONOUS_LOADING makes status Loading', ()=>{
      let result = reducer( undefined ,{type: ASYNCHRONOUS_LOADING })
      expect( result.status ).to.equal( 'Loading' )
      expect( result.items.length ).to.equal( 0 )
    })

    it('type ASYNCHRONOUS_ERROR makes status Error', ()=>{
      let result = reducer( undefined ,{type: ASYNCHRONOUS_ERROR })
      expect( result.status ).to.equal( 'Error' )
      expect( result.items.length ).to.equal( 0 )
    })

    it('type ASYNCHRONOUS_SUCCESS sets status and items', ()=>{
      let result = reducer( undefined ,{type: ASYNCHRONOUS_SUCCESS, payload :[ {a:1 }] })
      expect( result.status ).to.equal( 'Success' )
      expect( result.items.length ).to.equal( 1 )
    })
  })

})
