import {expect} from 'chai'

import {
  synchronousLoading,
  synchronousError,
  synchronousSuccess,
  SYNCHRONOUS_ERROR,
  SYNCHRONOUS_LOADING,
  SYNCHRONOUS_SUCCESS,
} from '../../client/ducks/SynchronousDuck'

import reducer from '../../client/ducks/SynchronousDuck' 

describe('SynchronousDuck', ()=>{

    describe('actions', ()=>{
    
        it('loading', ()=>{    
            expect( synchronousLoading().type).to.equal( SYNCHRONOUS_LOADING )
        })    

        it('error', ()=>{    
            expect( synchronousError().type).to.equal( SYNCHRONOUS_ERROR )
        }) 

        it('success', ()=>{    
            expect( synchronousSuccess().type).to.equal( SYNCHRONOUS_SUCCESS )
        })
    })

    describe('reducer', ()=>{

        it('initializes', ()=>{
            let result = reducer( undefined ,{type: 'unknown event' })
            expect( result ).to.equal( SYNCHRONOUS_LOADING )
        })

        it('passes through an unknown type', ()=>{
            let result = reducer( 123 ,{type: 'unknown event' })
            expect( result ).to.equal( 123 )
        })

        it('type SYNCHRONOUS_LOADING makes state SYNCHRONOUS_LOADING', ()=>{
            let result = reducer( 123 ,{type: SYNCHRONOUS_LOADING })
            expect( result ).to.equal( SYNCHRONOUS_LOADING )
        })

        it('type SYNCHRONOUS_ERROR makes state SYNCHRONOUS_ERROR', ()=>{
            let result = reducer( 123 ,{type: SYNCHRONOUS_ERROR })
            expect( result ).to.equal( SYNCHRONOUS_ERROR )
        })

        it('type SYNCHRONOUS_SUCCESS makes state SYNCHRONOUS_SUCCESS', ()=>{
            let result = reducer( 123 ,{type: SYNCHRONOUS_SUCCESS })
            expect( result ).to.equal( SYNCHRONOUS_SUCCESS )
        })

    })
})