
import React, {Component} from 'react';

import {
    SYNCHRONOUS_LOADING,
    SYNCHRONOUS_ERROR ,
    SYNCHRONOUS_SUCCESS,
  } from '../ducks/SynchronousDuck'

export default class Synchronous extends Component {
    render() {
        let {
            step,
            onError,
            onLoading,
            onSuccess,
        } = this.props

        let load_color    = 'black'
        let error_color   = 'black'
        let success_color = 'black'

        switch (step) {
            case SYNCHRONOUS_LOADING:
                step = "Loading"
                load_color = 'green'
                break
            case SYNCHRONOUS_ERROR:
                step = "Error"
                error_color = 'green'
                break
            case SYNCHRONOUS_SUCCESS:
                step = "Success"
                success_color = 'green'
                break
        }

        return (
            <div>
                <h1>Synchronous Component</h1>
                <h2>Status: {step}</h2>
                <ul>
                    <li onClick={onLoading} style={{cursor:'pointer', color: load_color}}    className='loading' > Loading </li>
                    <li onClick={onSuccess} style={{cursor:'pointer', color: success_color}} className='success' > Success </li>
                    <li onClick={onError}   style={{cursor:'pointer', color: error_color}}   className='error'   > Error </li>
                </ul>
            </div>);
    }
}
