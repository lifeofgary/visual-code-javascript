
import React, {Component} from 'react';

export default class PromiseComponent extends Component {
    render() {
        let {
            status,
            items,
            onError,
            onLoading,
            onSuccess,
        } = this.props

        items = Object.keys(items).map( key=><li key={key}>{key} = {items[key]}</li>)

        return (
        <div>
            <h1>Promise Component</h1>
            <h2>Status: {status}</h2>
            <ul>
                {items}
            </ul>
        </div>);
    }
}
