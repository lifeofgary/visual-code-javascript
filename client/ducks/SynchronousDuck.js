export const SYNCHRONOUS_ERROR           = Symbol('Synchronous has error')
export const SYNCHRONOUS_LOADING          = Symbol('Synchronous is loading')
export const SYNCHRONOUS_SUCCESS             = Symbol('Synchronous fetch success')

export const synchronousLoading = () => ({
    type: SYNCHRONOUS_LOADING,
    payload: SYNCHRONOUS_LOADING
})

export const synchronousError = () => ({
    type: SYNCHRONOUS_ERROR,
    payload: SYNCHRONOUS_ERROR
})

export const synchronousSuccess= () => ({
    type: SYNCHRONOUS_SUCCESS,
    payload: SYNCHRONOUS_SUCCESS
})

const initialState = SYNCHRONOUS_LOADING

export default ( state = initialState, action  ) => {
    let {type} = action

    switch(type) {
    case SYNCHRONOUS_ERROR:
        return type
    case SYNCHRONOUS_LOADING:
        return type
    case SYNCHRONOUS_SUCCESS:
        return type
    }
    return state
}
