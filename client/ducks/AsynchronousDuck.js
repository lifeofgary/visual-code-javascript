
export const ASYNCHRONOUS_ERROR   = Symbol('Promise has error')
export const ASYNCHRONOUS_LOADING = Symbol('Promise is loading')
export const ASYNCHRONOUS_SUCCESS = Symbol('Promise fetched')

export const asynchronousError = () => ({
    type: ASYNCHRONOUS_ERROR,
})

export const asynchronousLoading = () => ({
    type: ASYNCHRONOUS_LOADING,
})

export const asynchronousSuccess = payload=>({
    type: ASYNCHRONOUS_SUCCESS,
    payload,
})

export const  asynchronousFetch= url=>{
    return (dispatch) => {
        dispatch(asynchronousLoading())
        asynchronousFetchLogic( fetch(url), dispatch )
    }
}

export const asynchronousFetchLogic = (promise, dispatch) => {
    promise
        .then( response => {
            if (!response.ok) {
                throw Error(response.statusText)
            }
            dispatch(asynchronousLoading())
            return response
        })
        .then((response) => response.json())
        .then((items) => dispatch(asynchronousSuccess(items)))
        .catch(() => dispatch(asynchronousError()))
}

const initialState = { status: 'Loading', items: [] }

export default ( state = initialState, action  ) => {
    let {type, payload} = action

    switch(type) {
    case ASYNCHRONOUS_ERROR:
        return Object.assign( {}, {status: 'Error', items: []} )
    case ASYNCHRONOUS_LOADING:
        return Object.assign( {}, {status: 'Loading', items: []} )
    case ASYNCHRONOUS_SUCCESS:
        return Object.assign( {}, {status: 'Success', items: payload} )
    }
    return state
}
