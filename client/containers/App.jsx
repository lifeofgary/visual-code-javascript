import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import configureStore from '../store/configureStore';

import Asynchronous from '../components/Asynchronous.jsx'
import Synchronous from '../components/Synchronous.jsx'

import {
  synchronousLoading,
  synchronousError ,
  synchronousSuccess,
} from '../ducks/SynchronousDuck'

import {
  asynchronousLoading,
  asynchronousError,
  asynchronousSuccess,
  asynchronousFetch,
} from '../ducks/AsynchronousDuck'

class App extends React.Component {

  componentDidMount() {
    this.props.asynchronousFetch('http://echo.jsontest.com/key/value/one/two');
  }   

  render() {
    return (
      <div>
        <Asynchronous
          status={this.props.asynchronous.status}
          items={this.props.asynchronous.items}
          onLoading={this.props.asynchronousLoading}
          onError={this.props.asynchronousError}
          onSuccess={this.props.asynchronousSuccess}
          onFetch={this.props.asynchronous.asynchronousFetch}
        />
        <hr/>
        <Synchronous
          step={this.props.synchronous}
          onLoading={this.props.synchronousLoading}
          onError={this.props.synchronousError}
          onSuccess={this.props.synchronousSuccess}
          />
      </div>
      )
  }
}

const mapStateToProps = (state) =>
{
    return {
      asynchronous: state.asynchronous,
      synchronous:  state.synchronous,
    };
};

const mapDispatchToProps = {
  synchronousLoading,
  synchronousError ,
  synchronousSuccess,

  asynchronousLoading,
  asynchronousError ,
  asynchronousSuccess,
  asynchronousFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
