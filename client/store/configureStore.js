
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { combineReducers } from 'redux'

import asynchronous  from '../ducks/AsynchronousDuck.js'
import synchronous  from '../ducks/SynchronousDuck.js'

let rootReducer = combineReducers({
    asynchronous,
    synchronous,
})

export default function configureStore() {
    return createStore(
        rootReducer,
        applyMiddleware(thunk)
    )
}
