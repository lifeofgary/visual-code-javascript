```
yarn init temp
yarn add react
yarn add react-dom
yarn add react-redux
yarn add redux
yarn add redux-thunk
yarn add webpack
yarn add webpack-dev-server
yarn add babel-core
yarn add babel-loader
yarn add babel-preset-es2015
yarn add babel-preset-react
yarn add babel-preset-stage-3
yarn add mocha
# add test packages
yarn add enzyme --dev
yarn add karma --dev
yarn add karma-mocha-reporter --dev
yarn add chai --dev
yarn add chai-enzyme --dev
yarn add expect-enzyme --dev
yarn add sinon --dev
yarn add sinon-chai --dev
yarn add mocha-sinon --dev

```

In package.json add a start script.  Be sure to add a leading `,`
```
# package.json

...
  "scripts": {
    "start": "./node_modules/.bin/webpack-dev-server"
  }  
```
Create components/itemList.js
```
import React, { Component } from 'react';

class ItemList extends Component {
    constructor() {
        super();

        this.state = {
            items: [
                {
                    id: 1,
                    label: 'List item 1'
                },
                {
                    id: 2,
                    label: 'List item 2'
                },
                {
                    id: 3,
                    label: 'List item 3'
                },
                {
                    id: 4,
                    label: 'List item 4'
                }
            ],
            hasErrored: false,
            isLoading: false
        };
    }

    render() {
        if (this.state.hasErrored) {
            return <p>Sorry! There was an error loading the items</p>;
        }

        if (this.state.isLoading) {
            return <p>Loading…</p>;
        }

        return (
            <ul>
                {this.state.items.map((item) => (
                    <li key={item.id}>
                        {item.label}
                    </li>
                ))}
            </ul>
        );
    }
}

export default ItemList;
```

index.html
```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Todo</title>
</head>
<body>
  <div id="react"></div>
  <script src="bundle.js"></script>
</body>
</html>
```

webpack.config.js
```
var path = require("path");
module.exports = {
  entry: './components/app.jsx',
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/",
    filename: "bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-3']
        }
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  }
}
```



#References
[1] https://scotch.io/tutorials/setup-a-react-environment-using-webpack-and-babel
[2] 
https://medium.com/@stowball/a-dummys-guide-to-redux-and-thunk-in-react-d8904a7005d3
[3] https://medium.com/@dan_abramov/lint-like-it-s-2015-6987d44c5b48
[4] http://www.albertgao.xyz/2017/04/23/setting-up-react-with-babel-mocha-chai-enzyme-and-webpack/
[5] https://medium.com/@Jukejc/setting-up-karma-to-work-with-enzyme-mocha-and-webpack-in-2017-1ab0c2e9ef00

